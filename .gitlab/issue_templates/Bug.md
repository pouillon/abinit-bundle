Reporting a bug that is intrinsically related to the esl-bundle

NOTE: Please select option `Library Bug` when reporting bugs specific to a library

- [ ] Ensure the title is prefixed with `[bundle] `
- [ ] Add a description of the problem encountered with abinit-bundle
- [ ] Describe how the bundle was built
- [ ] Preferably attach your JHbuild `config.rc`
- [ ] If you have a fix, please let us know! :-)
